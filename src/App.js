import { useState, useEffect, useRef } from "react";
import { getEvents, searchEvent } from "./api/events";
import { EventContainer } from './components/Events/EventContainer';
import { SearchBar } from "./components/SearchBar";

function App() {
  const [events, setEvents] = useState([]);
  const [allEvents, setAllEvents] = useState([]);

  useEffect(() => {
    async function fetchData() {
      let _events = await getEvents();
      setAllEvents(_events);
      setEvents(_events);
    }
    fetchData();
  }, []);

  const onSearchEvent = (inputSearch) => setEvents(inputSearch === '' ? allEvents : searchEvent(allEvents, inputSearch))

  return (
    <div className="App text-[11px] tracking-wide bg-slate-100 p-3 md:p-28 min-h-screen text-gray-500 font-extralight flex justify-center">
      <div className="bg-white shadow-lg w-full md:w-[800px]  divide-y divide-gray-200">
        <SearchBar handleInputChange={onSearchEvent}/>
        <EventContainer events={events} />
      </div>
    </div>
  )
}

export default App;
