import moment from "moment";

export async function getEvents() {
  const url =
    "https://us-central1-cloud-project-e3d94.cloudfunctions.net/laker_events";
  const response = await fetch(url);
  const eventsFeched = await response.json();

  let listOfEvents = [];

  eventsFeched.forEach((event) => {
    const dateWhen = getPeriod(event.updated);

    let actualPeriod = listOfEvents.filter(
      (period) => period.period === dateWhen
    );

    if (!actualPeriod.length) {
      actualPeriod = {
        date: moment(event.updated).format("MMMM YY"),
        period: dateWhen,
        events: eventsFeched
          .filter((event) => getPeriod(event.updated) === dateWhen)
          .map((event) => {
            // return event
            return {
              id: event._id,
              name: event.name,
              background: event.background,
              dates: {
                start: moment(event.dates.start).format("dddd LT [|] DD MMM"),
              },
            };
          }),
      };

      listOfEvents = [...listOfEvents, actualPeriod];
    }
  });
  console.log(listOfEvents);
  listOfEvents.sort((a, b) => a.period - b.period);

  return listOfEvents;
}

const getPeriod = (date) => moment(date).format("YYMM");

export function searchEvent(events, search) {
  return events.map(period => {
    return {
      ...period,
      events: filterEventFromPeriod(period.events, search)
    }
  }).filter(period => period.events.length > 0)
}

function filterEventFromPeriod(events, search) {
  let eventsFiltered = events.filter(event =>
    event.name.toUpperCase().includes(search.toUpperCase())
  )
  // console.log(eventsFiltered.length > 0,  eventsFiltered);rr
  return eventsFiltered
}
