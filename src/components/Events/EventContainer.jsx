import { LineWave } from 'react-loader-spinner'
import { EventItem } from './EventItem'

export const EventContainer = ({ events }) => {
  return (
    <div className="px-4 md:px-10">
      {events.length === 0 && <LineWave
        color="blue"
        height={110}
        width={110}
        ariaLabel="three-circles-rotating"
      />}
      {events.map((period) => (
        <div className="py-4 md:py-10" key={period.date}>
          <div className="text-[18px] mb-10"> {period.date}</div>
          <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-10">
            { period.events.map((event) => (
              <EventItem event={event}/>
            )) }
          </div>
        </div> 
      ))}
    </div>
  )
}