export const EventItem = ({ event }) => {
  return (
    <div key={event.id}>
      <div className="font-semibold uppercase pb-2">{event.dates.start}</div>
      <img loading="lazy" src={event.background} className="w-full aspect-video object-cover" />
      <div className="text-[20px] mt-2">{event.name}</div>
    </div>
  )
}