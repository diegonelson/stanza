import { useRef } from "react";
import { IconSearch } from "./icons/search";

export const SearchBar = ({handleInputChange}) => {
  const inputSearch = useRef(null);

  const onChangeTextSearch = () => handleInputChange(inputSearch.current.value)
  return (
    <div className="px-4 md:px-6 py-4 flex items-center ">
      <IconSearch />
      <input
        ref={inputSearch}
        onChange={onChangeTextSearch}
        type="text"
        placeholder="SEARCH EVENT"
        className="w-full outline-0"
      />
    </div>
  )
}